# ---- Сборка ----
FROM ubuntu:latest AS builder


# Установка временной зоны
ENV TZ Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone


# Установка пакетов требующихся для сборки
RUN apt-get update &&\
    apt-get upgrade -y &&\
    apt-get install -y wget &&\
    apt-get install -y build-essential


# Рабочие каталоги для сборки
ENV NGINX_BUILD_ASSETS_DIR=/var/lib/docker-nginx \
    NGINX_BUILD_ROOT_DIR=/var/lib/docker-nginx/rootfs


# Смена рабочего каталога
WORKDIR ${NGINX_BUILD_ASSETS_DIR}


# Скачивание исходных текстов
RUN wget https://www.openssl.org/source/openssl-1.1.1g.tar.gz && tar xzvf openssl-1.1.1g.tar.gz && \
    wget http://www.zlib.net/zlib-1.2.11.tar.gz && tar xzvf zlib-1.2.11.tar.gz && \
    wget http://nginx.org/download/nginx-1.18.0.tar.gz && tar xzvf nginx-1.18.0.tar.gz


# Смена рабочего каталога
WORKDIR ${NGINX_BUILD_ASSETS_DIR}/nginx-1.18.0

# Сборка 
RUN ./configure --prefix=/usr/share/nginx \
            --sbin-path=/usr/sbin/nginx \
            --modules-path=/usr/lib/nginx/modules \
            --conf-path=/etc/nginx/nginx.conf \
            --error-log-path=/var/log/nginx/error.log \
            --http-log-path=/var/log/nginx/access.log \
            --pid-path=/run/nginx.pid \
            --lock-path=/var/lock/nginx.lock \
            --user=www-data \
            --group=www-data \
            --build=test_task \
            --without-http_rewrite_module \
            --with-http_gunzip_module \
  	        --with-http_gzip_static_module \
            --with-http_ssl_module \
            --with-http_stub_status_module \
            --with-openssl=../openssl-1.1.1g \
            --with-zlib=../zlib-1.2.11 \
            --with-cc-opt='-g -O2' && \
    make && \
    make DESTDIR=${NGINX_BUILD_ROOT_DIR} install

COPY entrypoint.sh ${NGINX_BUILD_ROOT_DIR}/sbin/entrypoint.sh

COPY nginx.conf ${NGINX_BUILD_ROOT_DIR}/etc/nginx/

RUN chmod 755 ${NGINX_BUILD_ROOT_DIR}/sbin/entrypoint.sh


# ---- Запуск ----
FROM ubuntu:latest


# Утсновка временной зоны
ENV TZ Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone


ENV NGINX_USER=www-data \
    NGINX_SITECONF_DIR=/etc/nginx/sites-enabled \
    NGINX_LOG_DIR=/var/log/nginx \
    NGINX_TEMP_DIR=/var/lib/nginx

COPY --from=builder /var/lib/docker-nginx/rootfs /

        
EXPOSE 80


# ENTRYPOINT ["/sbin/entrypoint.sh"]

CMD ["/usr/sbin/nginx", "-c", "/etc/nginx/nginx.conf", "-g", "daemon off;"]
